package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;



public class RegistrationActivity extends AppCompatActivity {

    public static String EXTRA_NAME = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    public void registerUser(View view){
        EditText e = (EditText) findViewById(R.id.edit_name);
        String fullName = e.getText().toString();
        if(fullName.length() == 0){
            e.setError(getString(R.string.reg_full_name_error));
        }else{
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(EXTRA_NAME, fullName);
            startActivity(intent);
        }

    }
}
