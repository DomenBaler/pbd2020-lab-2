package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class ProfileActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        String fullName = intent.getStringExtra(RegistrationActivity.EXTRA_NAME);
        TextView t = (TextView) findViewById(R.id.your_name);
        t.setText(fullName);

        View v = findViewById(R.id.show_msg);

        v.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View v) {
            Context context = getApplicationContext();
            EditText editMsg = (EditText) findViewById(R.id.msg);
            String msg = editMsg.getText().toString();
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, msg, duration);
            toast.show();}});
    }
}
